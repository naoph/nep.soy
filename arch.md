# Connect to the network

    $ iwctl
	device list
	station [device] scan
	station [device] get-networks
	station [device] connect [ssid]

# Enable NTP

    $ timedatectl set-ntp true

# Partition disc

    $ cfdisk [disk]
	gpt
	EFI partition: EFI System / 1G
	root partition: Linux filesystem / remainder

# Format partitions

    $ mkfs.fat -F 32 [EFI partition]
    $ cryptsetup luksFormat [root partition]
    $ cryptsetup open [root partition] root
    $ mkfs.btrfs /dev/mapper/root
    $ mount /dev/mapper/root /mnt
    $ mkdir /mnt/boot
    $ mount [EFI partition] /mnt/boot

# Bootstrap

    $ arch.sh

# fstab

    $ genfstab -U /mnt >> /mnt/etc/fstab

# chroot

    $ arch-chroot /mnt

# Time

    $ ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
    $ hwclock --systohc

# Locale

    $ nvim /etc/locale.gen
	Uncomment en_US.UTF-8 UTF-8
    $ locale-gen
    $ echo 'LANG=en_US.UTF-8' > /etc/locale.conf

# Hosts

    $ echo [hostname] > /etc/hostname
    $ nvim /etc/hosts
	127.0.0.1   localhost
	::1	    localhost
	127.0.1.1   [hostname]

# Initramfs

    $ nvim /etc/mkinitcpio.conf
	Scroll to HOOKS, insert "encrypt" between block and filesystems
    $ mkinitcpio -P

# Users
    $ useradd -G wheel -s /bin/zsh -m [username]
    $ passwd [username]
    $ passwd root
    $ visudo

# Install bootloader

    $ grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB

# Configure bootloader

    Find encrypted root partition's UUID in `ls -l /dev/disk/by-uuid`
    $ nvim /etc/default/grub
	GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet cryptdevice=UUID=[UUID]:root root=/dev/mapper/root"
    $ grub-mkconfig -o /boot/grub/grub.cfg

# Finishing touches

    $ systemctl enable gdm
    $ systemctl enable NetworkManager

# Reboot

    $ exit
    $ umount -R /mnt
    $ cryptsetup close root
    $ sync
    $ reboot
