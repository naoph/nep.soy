# nep.soy

Remember to add this to your nginx server block:

    location ~ /\.git {
        deny all;
    }
